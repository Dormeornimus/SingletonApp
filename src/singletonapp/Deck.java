/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singletonapp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Dorme Ornimus
 */
public class Deck {
    private static List<Card> cards;
    private static Deck uniqueInstance;
    
    private Deck( ) {
    cards = new ArrayList<Card>( );

    // build the deck
    Suit[] suits = {Suit.SPADES, Suit.HEARTS, Suit.CLUBS, Suit.DIAMONDS};
    for(Suit suit: suits) {
      for(int i = 2; i <= 14; i++) {
        cards.add(new Card(suit, i));
      }
    }

    // shuffle it!
    Collections.shuffle(cards, new Random( ));
  }

  public void print( ) {
    for(Card card: cards) {
      card.print( );
    }
  }
    
public static Deck getInstance(){
        if(uniqueInstance == null){
            uniqueInstance = new Deck();
        }
        return uniqueInstance;
}

}

